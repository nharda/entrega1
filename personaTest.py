  import unittest 
from persona import Paciente
from persona import Medico

class TestPaciente (unittest.TestCase):
    def test_ver_historial_clinico01 (self):
        persona01= Paciente("Juana", "Gómez Rodríguez", "02/06/1989", "02666433G", "fractura")
        self.assertEqual(persona01._hisitorial_clinico, "fractura")

class TestMedico(unittest.TestCase):
    def test_consultar_agenda01(self):
        persona02= Medico("Juana", "Gómez Rodríguez", "02/06/1989", "02666433G", "fractura", "14 febrero")
        agenda= persona02.consultar_agenda()
        self.assertEqual(agenda, "Especialidad: Traumatología, Citas: 14 febrero")