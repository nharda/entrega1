import unittest
from coche import Coche

class TestFraccion (unittest.TestCase):
    def test_acelera (self):
        c1= Coche ("Candela", "Blanco", "Citroen", "C4", "8967NJC", 10)
        c2= Coche ("Javi", "Negro", "Mercedes", "Clase E", "2222CSJ", 29)

        acelera= c1.acelera(10)
        self.assertEqual (c1.velocidad,20)

    def test_frena (self):
        c1= Coche ("Candela", "Blanco", "Citroen", "C4", "8967NJC", 10)
        c2= Coche ("Javi", "Negro", "Mercedes", "Clase E", "2222CSJ", 29)

        frena= c2.frena(-10)
        self.assertEqual (c2.velocidad,19)

unittest.main ()